export class OdndActorSheet extends ActorSheet {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["odnd", "sheet", "actor"],
            template: "systems/odnd/templates/actor/actor-sheet.html",
            width: 200,
            height: 200,
            tabs: []
        });
    }

    /** @override */
    get template() {
        return `systems/odnd/templates/actor/actor-character-sheet.html`;
    }

    /** @override */
    getData() {
        const context = super.getData();

        const actorData = context.actor.data.toObject(false);

        context.data = actorData.data;
        context.flags = actorData.flags;

        if (actorData.type == 'character') {

        }

        return context;
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        //-----
        if (!this.isEditable) {
            return;
        }
    }
}