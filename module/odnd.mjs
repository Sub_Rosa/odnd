// Insert imports
import { OdndActorSheet } from './sheets/actor-sheet.mjs';
Hooks.once('init', function() {
    console.log('Hi, world');
    console.log('Why oh why');
    // Define custom document classes
    CONFIG.Actor.documentClass = OdndActor;
    //CONFIG.Item.documentClass = OdndItem;

    // Register sheets
    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet("odnd", OdndActorSheet, {makeDefault: true})
    //Items.unregisterSheet("core", ItemSheet);

    // Support for handlebars partials
    return preloadHandlebarsTemplates();
});